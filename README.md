# README #

### What is this for? ###
* Providing a centralised location to manage the dreaded 'required' tags
* It's an AWS example but should be hella portable!

### default-tags ###
* An example snippet to attach your 'organisation/department' default tags to an object in an s3 bucket.

### use-default-tags ###
* An example snippet to access those tags and make them the default for that provider.
* The second provider aliased as 'tags-config' and sourcing the data from that alias is necessary to avoid a cycle error.
