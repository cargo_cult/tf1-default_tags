
terraform {

  backend "s3" {
    bucket = "culted"
    key    = "terraform/default-tags.tf"
    region = "eu-west-2"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.46.0"
    }
  }
}

provider "aws" {
  region = "eu-west-2"
}

resource "aws_s3_bucket_object" "tags-config" {
  bucket  = "culted"
  content = "hello!"
  key     = "tags-config"

  tags = {
    CostCentre = "Whatever"
    Owner      = "john@doe.net"
  }
}
