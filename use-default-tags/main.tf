
terraform {

  backend "s3" {
    bucket = "culted"
    key    = "terraform/use-default-tags.tf"
    region = "eu-west-2"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.46.0"
    }
  }
}

data "aws_s3_bucket_object" "default-tags" {
  bucket   = "culted"
  key      = "tags-config"
  provider = aws.tags-config
}

provider "aws" {
  alias  = "tags-config"
  region = "eu-west-2"
}

provider "aws" {
  region = "eu-west-2"
  default_tags {
    tags = data.aws_s3_bucket_object.default-tags.tags
  }
}

resource "aws_s3_bucket_object" "use-tags-config" {
  bucket  = "culted"
  content = "hello!"
  key     = "use-tags-config"

  tags = {
    NonDefaultTag = "Wherever"
  }
}
